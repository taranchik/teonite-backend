# TEONITE Backend App

![Project Image](https://i.imgur.com/KbZfy0H.png)

> TEONITE Blog stats

---

## How To Use

### Installation

1. Clone the repositry
```
git clone https://gitlab.com/taranchik/teonite-backend
```

2. Build, create, start, and attach to containers for a service
```
docker-compose up
```

### Usage

In order to fetch some data you are able to execute the following commands:

1. Fetch general stats of the teonite-blog articles
```
curl http://localhost:8080/stats/
```

2. Fetch authors of the TEONITE blog articles
```
curl http://localhost:8080/authors/
```

3. Fetch stats of the specific author
```
curl http://localhost:8080/stats/antekmilkowski/
```

### WARNING
Be careful and do not forget to type slash at the end of the line otherwise command could not work (in case if you are executing it directly from the terminal). The application is configured to fetch data directly from the addresses `/stats/`, `/stats/<author>/`, `/authors/`. In browser the slash is added automatically.

---

## References
[TEONITE Blog](https://teonite.com/blog/)

[Back To The Top](#teonite-backend-app)