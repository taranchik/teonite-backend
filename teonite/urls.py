from django.urls import path

from . import views

urlpatterns = [
    path('stats/', views.stats, name='stats'),
    path('stats/<authorKey>/', views.author_name, name='author_name'),
    path('authors/', views.authors, name='authors'),
]
